import std.algorithm : canFind, filter;
import std.concurrency : LinkTerminated, ownerTid, receive, send, spawnLinked;
import std.file : exists, getcwd, readText;
import std.format : format;
import std.process : Config, environment, execute, pipeProcess, ProcessPipes, Redirect, wait;
import std.range : front;
import std.stdio : File, KeepTerminator, stderr, stdout, write, writeln;
import std.string : strip;
import thepath : Path;

enum DLANG_DEFAULT = "ldc-1.35.0";

auto insertAfter(string[] array, string needle, string[] addition)
{
    string[] result;
    foreach (s; array)
    {
        result ~= s;
        if (s == needle)
        {
            result ~= addition;
        }
    }
    return result;
}

string getDlangVersion(Path projectDirectory)
{
    auto dlangVersionFile = projectDirectory.searchFileUp(".dlang-version");
    string result = DLANG_DEFAULT;
    if (dlangVersionFile.isNull)
    {
        stderr.writeln("No .dlang-version file found");
    } else {
        stderr.writeln("Using ", dlangVersionFile.get, " for dlang config");
        result = dlangVersionFile.get.readFileText.strip;
    }
    stderr.writeln("Resulting in ", result);
    return result;
}

struct DataForStdout
{
    string line;
}

struct DataForStderr
{
    string line;
}

void read(T)(shared(File delegate()) get)
{
    auto f = get();
    foreach (line; f.byLineCopy(KeepTerminator.yes))
    {
        ownerTid.send(T(line.idup));
    }
}

int main(string[] args)
{
    const projectDirectory = Path.current.searchFileUp("dub.sdl").get().parent();
    const dlangVersion = projectDirectory.getDlangVersion;
    const home = environment["HOME"];
    auto command = [
      [
        format!("%s/dlang/%s/osx/bin/dub")(home, dlangVersion),
        format!("%s/dlang/%s/bin/dub")(home, dlangVersion),
      ]
        .filter!(s => s.exists)
        .front
    ];
    auto compiler =
        [
            format!("%s/dlang/%s/osx/bin/dmd")(home, dlangVersion),
            format!("%s/dlang/%s/bin/ldc2")(home, dlangVersion),
        ]
        .filter!(s => s.exists)
        .front;
    auto rest = args[1..$];
    auto additionalFlags = ["--compiler=%s".format(compiler)];
    rest = rest.insertAfter("build", additionalFlags);
    rest = rest.insertAfter("run", additionalFlags);
    rest = rest.insertAfter("test", additionalFlags);
    command ~= rest;

    auto pipes = command.pipeProcess(Redirect.all, ["MACOSX_DEPLOYMENT_TARGET": "11"], Config.none, projectDirectory.toString);
    auto getStdout() => pipes.stdout; // strange workaround as more direct ways do not work for me
    auto getStderr() => pipes.stderr;
    spawnLinked(&read!(DataForStderr), cast(shared)&getStderr);
    spawnLinked(&read!(DataForStdout), cast(shared)&getStdout);

    int linkTerminated = 0;
    while (linkTerminated < 2)
    {
        receive(
          (DataForStdout d) => stdout.write(d.line),
          (DataForStderr d) { if (!d.line.canFind("no platform load command found in")) { stderr.write(d.line); } },
          (LinkTerminated l) => linkTerminated++,
        );
    }
    return pipes.pid.wait;
}
