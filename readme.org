* D - Make working with dub simpler
** Features
- can be called in any subdirectory of a dub project.
- understands .dlang-version in any dominating folder of the dub
  project. So you can have a user default in =$HOME= and override this
  if needed on a per project basis.

** Usage
Just =dub build= and put the resulting binary (=out/main/d=) somewhere
in your =$PATH=.
